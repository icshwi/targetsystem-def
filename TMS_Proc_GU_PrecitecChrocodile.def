# TMS_Proc_GU_PrecitecChrocodile
# 2022-11-11
#
############################
#  DEFAULTS
############################ 
#set_defaults(ARCHIVE=True)

############################
#  STATUS BLOCK
############################ 
define_status_block()

#   Device Data
# -------------------------------------
add_digital("Fault_Ch1_CtrlAI_Act",              PV_DESC="Fault Ch1 CtrlAnalog In Act",                                PV_ONAM="NOK", PV_ZNAM="OK",                 VALIDITY_CONDITION=False, ARCHIVE=True)
add_minor_alarm("Ch1_AI_Overrange",    "NOK",    PV_DESC="Ch1 Analog In Overrange",                                    PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="Fault_Ch1_CtrlAI_Act", ARCHIVE=True)
add_minor_alarm("Ch1_AI_Underrange",   "NOK",    PV_DESC="Ch1 Analog In Underrange",                                   PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="Fault_Ch1_CtrlAI_Act", ARCHIVE=True)
add_analog("L1_Raw_AI",                "REAL",   PV_DESC="L1 Raw Analog In",                            PV_EGU="mm",                                                                              VALIDITY_PV="Fault_Ch1_CtrlAI_Act", ARCHIVE=True)

add_digital("Fault_Ch2_CtrlAI_Act",              PV_DESC="Fault Ch2 CtrlAnalog In Act",                                PV_ONAM="NOK", PV_ZNAM="OK",                 VALIDITY_CONDITION=False, ARCHIVE=True)
add_minor_alarm("Ch2_AI_Overrange",    "NOK",    PV_DESC="Ch2 Analog In Overrange",                                    PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="Fault_Ch2_CtrlAI_Act", ARCHIVE=True)
add_minor_alarm("Ch2_AI_Underrange",   "NOK",    PV_DESC="Ch2 Analog In Underrange",                                   PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="Fault_Ch2_CtrlAI_Act", ARCHIVE=True)
add_analog("L2_Raw_AI",                "REAL",   PV_DESC="L2 Raw Analog In",                            PV_EGU="mm",                                                                              VALIDITY_PV="Fault_Ch2_CtrlAI_Act", ARCHIVE=True)

add_minor_alarm("Device_Err_TCPIP",    "NOK",    PV_DESC="Device Err TCPIP_",                                          PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True, ARCHIVE=True)
add_digital("Network_Err",                       PV_DESC="Network Err",                                                PV_ONAM="NOK", PV_ZNAM="OK",                 VALIDITY_CONDITION=False, ARCHIVE=True)
add_minor_alarm("Ch1_TCPIP_Overrange", "NOK",    PV_DESC="Ch1 TCPIP_ Overrange",                                       PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="Network_Err", ARCHIVE=True)
add_minor_alarm("Ch1_TCPIP_Underrange", "NOK",   PV_DESC="Ch1 TCPIP_ Underrange",                                      PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="Network_Err", ARCHIVE=True)
add_analog("L1_Raw_TCPIP",             "REAL",   PV_DESC="L1 Raw TCPIP_",                               PV_EGU="mm",                                                                              VALIDITY_PV="Network_Err", ARCHIVE=True)
add_minor_alarm("Ch2_TCPIP_Overrange", "NOK",    PV_DESC="Ch2 TCPIP_ Overrange",                                       PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="Network_Err", ARCHIVE=True)
add_minor_alarm("Ch2_TCPIP_Underrange", "NOK",   PV_DESC="Ch2 TCPIP_ Underrange",                                      PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="Network_Err", ARCHIVE=True)
add_analog("L2_Raw_TCPIP",             "REAL",   PV_DESC="L2 Raw TCPIP_",                               PV_EGU="mm",                                                                              VALIDITY_PV="Network_Err", ARCHIVE=True)

#   Device Control
# -------------------------------------
add_digital("Ctrl_SYNCIN_Act",                   PV_DESC="Ctrl SYNCIN Act",                                            PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)

#   Processed Data
# -------------------------------------
add_digital("MaskMode_Raw_Act",                  PV_DESC="MaskMode Raw Act",                                           PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("RawData_Valid",                     PV_DESC="RawData Valid",                                              PV_ONAM="OK",  PV_ZNAM="NOK",                VALIDITY_CONDITION=True, ARCHIVE=True)
add_analog("L1_Raw_Used",              "REAL",   PV_DESC="L1 Raw Used",                                 PV_EGU="mm",                                                                              VALIDITY_PV="RawData_Valid", ARCHIVE=True)
add_analog("L2_Raw_Used",              "REAL",   PV_DESC="L2 Raw Used",                                 PV_EGU="mm",                                                                              VALIDITY_PV="RawData_Valid", ARCHIVE=True)
add_string("Text_RawUsed_Source", 39,            PV_DESC="Text RawUsed Source", ARCHIVE=False)
add_analog("L1_Raw_Mask_R",            "REAL",   PV_DESC="L1 Raw Mask R",                               PV_EGU="mm", ARCHIVE=True)
add_analog("L2_Raw_Mask_R",            "REAL",   PV_DESC="L2 Raw Mask R",                               PV_EGU="mm", ARCHIVE=True)

add_analog("L1_SampleHold",            "REAL",   PV_DESC="L1 SampleHold",                               PV_EGU="mm", ARCHIVE=True)
add_analog("L2_SampleHold",            "REAL",   PV_DESC="L2 SampleHold",                               PV_EGU="mm", ARCHIVE=True)
add_digital("SampleHold_Act",                    PV_DESC="SampleHold Act",                                             PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_analog("L1_OffsetManu_RB",         "REAL",   PV_DESC="L1 OffsetManu RB",                            PV_EGU="mm", ARCHIVE=True)
add_analog("L2_OffsetManu_RB",         "REAL",   PV_DESC="L2 OffsetManu RB",                            PV_EGU="mm", ARCHIVE=True)
add_digital("ManuOffset_Act",                    PV_DESC="ManuOffset Act",                                             PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_string("Text_Offset_Source", 39,             PV_DESC="Text Offset Source", ARCHIVE=False)

add_digital("MaskMode_Calc_Act",                 PV_DESC="MaskMode Calc Act",                                          PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("CalcData_Valid",                    PV_DESC="CalcData Valid",                                             PV_ONAM="OK",  PV_ZNAM="NOK",                VALIDITY_CONDITION=True, ARCHIVE=True)
add_analog("L1_Calc_Used",             "REAL",   PV_DESC="L1 Calc Used",                                PV_EGU="mm",                                                                              VALIDITY_PV="CalcData_Valid", ARCHIVE=True)
add_analog("L2_Calc_Used",             "REAL",   PV_DESC="L2 Calc Used",                                PV_EGU="mm",                                                                              VALIDITY_PV="CalcData_Valid", ARCHIVE=True)
add_string("Text_CalcUsed_Source", 39,           PV_DESC="Text CalcUsed Source", ARCHIVE=False)
add_analog("L1_Calc_Mask_R",           "REAL",   PV_DESC="L1 Calc Mask R",                              PV_EGU="mm", ARCHIVE=True)
add_analog("L2_Calc_Mask_R",           "REAL",   PV_DESC="L2 Calc Mask R",                              PV_EGU="mm", ARCHIVE=True)

add_minor_alarm("Fault",               "NOK",    PV_DESC="Fault",                                                      PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True, ARCHIVE=True)
add_digital("MaskMode_Act",                      PV_DESC="MaskMode Act",                                               PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)


############################
#  COMMAND BLOCK
############################ 
define_command_block()
add_digital("Cmd_Sync",                          PV_DESC="Cmd Sync",                                                   PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)

#   Masking - Raw Val
# -------------------------------------
add_digital("Cmd_MaskRaw_Apply",                 PV_DESC="Cmd MaskRaw Apply",                                          PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)

#   Referencing
# -------------------------------------
add_digital("Cmd_SampleHold",                    PV_DESC="Cmd SampleHold",                                             PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("Cmd_SampleRelease",                 PV_DESC="Cmd SampleRelease",                                          PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("Cmd_SelectOffsetManu",              PV_DESC="Cmd SelectOffsetManu",                                       PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("Cmd_SelectOffsetSamp",              PV_DESC="Cmd SelectOffset Sampled",                                   PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)

#   Masking - Calc Val
# -------------------------------------
add_digital("Cmd_MaskCalc_Apply",                PV_DESC="Cmd MaskCalc Apply",                                         PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#   Masking - Raw Val
# -------------------------------------
add_digital("MaskMode_Raw_S",                    PV_DESC="MaskMode Raw Param SetVal",                                  PV_ONAM="True", PV_ZNAM="False", ARCHIVE=False)
add_analog("L1_Raw_Mask_S",            "REAL",   PV_DESC="L1 Raw Mask Param SetVal",                    PV_EGU="mm", ARCHIVE=False)
add_analog("L2_Raw_Mask_S",            "REAL",   PV_DESC="L2 Raw Mask Param SetVal",                    PV_EGU="mm", ARCHIVE=False)

#   Referencing
# -------------------------------------
add_analog("L1_OffsetManu_S",          "REAL",   PV_DESC="L1 OffsetManu Param SetVal",                  PV_EGU="mm", ARCHIVE=False)
add_analog("L2_OffsetManu_S",          "REAL",   PV_DESC="L2 OffsetManu Param SetVal",                  PV_EGU="mm", ARCHIVE=False)

#   Masking - Calc Val
# -------------------------------------
add_digital("MaskMode_Calc_S",                   PV_DESC="MaskMode Calc Param SetVal",                                 PV_ONAM="True", PV_ZNAM="False", ARCHIVE=False)
add_analog("L1_Calc_Mask_S",           "REAL",   PV_DESC="L1 Calc Mask Param SetVal",                   PV_EGU="mm", ARCHIVE=False)
add_analog("L2_Calc_Mask_S",           "REAL",   PV_DESC="L2 Calc Mask Param SetVal",                   PV_EGU="mm", ARCHIVE=False)

