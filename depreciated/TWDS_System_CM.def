################################  PLC Factory Def File  ######################################
##                                                                                          ##
##								TWDS1000:: (System)											##
##							Condition Monitoring Function									##
##############################################################################################
# Author:  Evan Foy 
# Date:    2021-10-04
# Version: v1.0
# Notes: 
#	* Max length of PV record name = 20 charaters
#	* Max length of PV record description = 40 charaters

############################
#  STATUS BLOCK
############################ 
define_status_block()
# Condition Monitoring Analysis (CMA) - Configuration
add_analog("CMA_NChannels",				"INT",		PV_DESC="CMA - Input channels processed",				PV_EGU="Channels")
add_analog("CMA_DownsampFctr",			"INT",		PV_DESC="CMA - Downsampling Factor",					PV_EGU="-")
add_analog("CMA_NSamp_InputArr",		"INT",		PV_DESC="CMA - Samples in Input Array",					PV_EGU="Samples")
add_analog("CMA_FFTLength",				"INT",		PV_DESC="CMA - FFT Length",								PV_EGU="Samples")
add_analog("CMA_WindowLength",			"INT",		PV_DESC="CMA - Window Length",							PV_EGU="Samples")
add_analog("CMA_NOverlap",				"INT",		PV_DESC="CMA - Samples Overlap",						PV_EGU="Samples")
add_analog("CMA_NBufferLength",			"INT",		PV_DESC="CMA - Buffer Length",							PV_EGU="Samples")
add_analog("CMA_NSpectrumResult",		"INT",		PV_DESC="CMA - Length of Spectrum result",				PV_EGU="Bins")
add_analog("CMA_NQuantilBins_Mx",		"INT",		PV_DESC="CMA - Max bins in Quantiles calculation",		PV_EGU="Bins")
add_analog("CMA_SampRate_In",			"REAL",		PV_DESC="CMA - Sample rate to analysis",				PV_EGU="sps")
add_analog("CMA_Freq_IntRMS_LL",		"REAL",		PV_DESC="CMA - Freq range Lower IntRMS analysis",			PV_EGU="Hz")
add_analog("CMA_Freq_IntRMS_HL",		"REAL",		PV_DESC="CMA - Freq range Upper IntRMS analysis",			PV_EGU="Hz")

# Condition Monitoring Analysis (CMA) - Vibration Assessment Settings Readback/Prepared
add_analog("Acc_RMS_HL1_RB",			"REAL",		PV_DESC="Accel (rms) Vib. Class HL1 RB Prepared",		PV_EGU="m/s2")
add_analog("Acc_RMS_HL2_RB",			"REAL",		PV_DESC="Accel (rms) Vib. Class HL2 RB Prepared",		PV_EGU="m/s2")
add_analog("Acc_RMS_HL3_RB",			"REAL",		PV_DESC="Accel (rms) Vib. Class HL3 RB Prepared",		PV_EGU="m/s2")
add_analog("Vel_RMS_HL1_RB",			"REAL",		PV_DESC="Vel (rms) Vib. Class HL1 RB Prepared",			PV_EGU="m/s")
add_analog("Vel_RMS_HL2_RB",			"REAL",		PV_DESC="Vel (rms) Vib. Class HL2 RB Prepared",			PV_EGU="m/s")
add_analog("Vel_RMS_HL3_RB",			"REAL",		PV_DESC="Vel (rms) Vib. Class HL3 RB Prepared",			PV_EGU="m/s")
add_analog("Disp_RMS_HL1_RB",			"REAL",		PV_DESC="Disp (rms) Vib. Class HL1 RB Prepared",		PV_EGU="m")
add_analog("Disp_RMS_HL2_RB",			"REAL",		PV_DESC="Disp (rms) Vib. Class HL2 RB Prepared",		PV_EGU="m")
add_analog("Disp_RMS_HL3_RB",			"REAL",		PV_DESC="Disp (rms) Vib. Class HL3 RB Prepared",		PV_EGU="m")

# Condition Monitoring Analysis (CMA) - Vibration Assessment Settings Set/Applied
add_analog("Acc_RMS_HL1_Set",			"REAL",		PV_DESC="Accel (rms) Vib. Class HL1 Set",				PV_EGU="m/s2")
add_analog("Acc_RMS_HL2_Set",			"REAL",		PV_DESC="Accel (rms) Vib. Class HL2 Set",				PV_EGU="m/s2")
add_analog("Acc_RMS_HL3_Set",			"REAL",		PV_DESC="Accel (rms) Vib. Class HL3 Set",				PV_EGU="m/s2")
add_analog("Vel_RMS_HL1_Set",			"REAL",		PV_DESC="Vel (rms) Vib. Class HL1 Set",					PV_EGU="m/s")
add_analog("Vel_RMS_HL2_Set",			"REAL",		PV_DESC="Vel (rms) Vib. Class HL2 Set",					PV_EGU="m/s")
add_analog("Vel_RMS_HL3_Set",			"REAL",		PV_DESC="Vel (rms) Vib. Class HL3 Set",					PV_EGU="m/s")
add_analog("Disp_RMS_HL1_Set",			"REAL",		PV_DESC="Disp (rms) Vib. Class HL1 Set",				PV_EGU="m")
add_analog("Disp_RMS_HL2_Set",			"REAL",		PV_DESC="Disp (rms) Vib. Class HL2 Set",				PV_EGU="m")
add_analog("Disp_RMS_HL3_Set",			"REAL",		PV_DESC="Disp (rms) Vib. Class HL3 Set",				PV_EGU="m")

# Condition Monitoring Analysis (CMA) - Vibration Assessment - Classification Args Applied
add_digital("CMA_VAclas_Args",					    PV_DESC="CMA Vib Assessmnt Class Args Applied")

# Condition Monitoring Analysis (CMA) - Vibration Assessment - TDU Main Bearings
add_analog("ISO10816_TduUppMBrg",		"INT",		PV_DESC="ISO10816 Vibr Class of TDU Upp Main Brg",		PV_EGU="Enum")
add_analog("ISO10816_TduLowMBrg",		"INT",		PV_DESC="ISO10816 Vibr Class of TDU Low Main Brg",		PV_EGU="Enum")

# Condition Monitoring Analysis (CMA) - Frequency Analysis Readbacks
add_analog("CMA_Qntl_nApData_RB",		"REAL",		PV_DESC="CMA Quantiles Append Data Readback",				PV_EGU="Samples")
add_digital("CMA_QntlRSD_RB",				    	PV_DESC="CMA Quantiles ResetData Readback")

add_analog("CMA_FA_Coeff_BaseRB",		"REAL",		PV_DESC="CMA FreqAnalysis Coeff Base RB",		PV_EGU="-")
add_analog("CMA_FA_Coeff_HL1_RB",		"REAL",		PV_DESC="CMA FreqAnalysis Coeff HL1 RB",		PV_EGU="pu")
add_analog("CMA_FA_Coeff_HL2_RB",		"REAL",		PV_DESC="CMA FreqAnalysis Coeff HL2 RB",		PV_EGU="pu")
add_digital("CMA_FA_TeachTim_Act",					PV_DESC="CMA FreqAnalysis TeachTimer Act")

add_analog("CMA_FA_Coeff_Base",		"REAL",		PV_DESC="CMA FreqAnalysis Coeff Base Applied",			PV_EGU="-")
add_analog("CMA_FA_Coeff_HL1",		"REAL",		PV_DESC="CMA FreqAnalysis Coeff HL1 Applied",			PV_EGU="pu")
add_analog("CMA_FA_Coeff_HL2",		"REAL",		PV_DESC="CMA FreqAnalysis Coeff HL2 Applied",			PV_EGU="pu")
add_digital("CMA_FA_clasArgs_Set",				PV_DESC="CMA FreqAnalysis Class Args Applied")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

# Condition Monitoring Analysis (CMA) - Vibration Assessment Commands
add_digital("cmd_CMA_VA_Config",						PV_DESC="Cmd CMA Vib Assessmnt Class Config")

# Condition Monitoring Analysis (CMA) - Frequency Analysis Commands
add_digital("cmd_CMA_QntlRSD_Tog",						PV_DESC="Cmd CMA Quantiles ResetData Toggle")
                 
add_digital("cmd_CMA_FA_Config",						PV_DESC="Cmd CMA Freq Analysis Class Config")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

# Condition Monitoring Analysis (CMA) - Vibration Assessment Settings
add_analog("Acc_RMS_HL1",			"REAL",		PV_DESC="Accel (rms) Vib. Class HL1 Setpoint",			PV_EGU="m/s2")
add_analog("Acc_RMS_HL2",			"REAL",		PV_DESC="Accel (rms) Vib. Class HL2 Setpoint",			PV_EGU="m/s2")
add_analog("Acc_RMS_HL3",			"REAL",		PV_DESC="Accel (rms) Vib. Class HL3 Setpoint",			PV_EGU="m/s2")
add_analog("Vel_RMS_HL1",			"REAL",		PV_DESC="Vel (rms) Vib. Class HL1 Setpoint",			PV_EGU="m/s")
add_analog("Vel_RMS_HL2",			"REAL",		PV_DESC="Vel (rms) Vib. Class HL2 Setpoint",			PV_EGU="m/s")
add_analog("Vel_RMS_HL3",			"REAL",		PV_DESC="Vel (rms) Vib. Class HL3 Setpoint",			PV_EGU="m/s")
add_analog("Disp_RMS_HL1",			"REAL",		PV_DESC="Disp (rms) Vib. Class HL1 Setpoint",			PV_EGU="m")
add_analog("Disp_RMS_HL2",			"REAL",		PV_DESC="Disp (rms) Vib. Class HL2 Setpoint",			PV_EGU="m")
add_analog("Disp_RMS_HL3",			"REAL",		PV_DESC="Disp (rms) Vib. Class HL3 Setpoint",			PV_EGU="m")

# Condition Monitoring Analysis (CMA) - Frequency Analysis Settings
add_analog("CMA_Qntl_nApData_S",		"REAL",		PV_DESC="CMA FreqAn. Quantiles Append Data SP",				PV_EGU="Samples")
add_analog("CMA_FA_Coeff_Base_S",		"REAL",		PV_DESC="CMA FreqAnalysis Coeff Base Setpoint",				PV_EGU="pu")
add_analog("CMA_FA_Coeff_HL1_S",		"REAL",		PV_DESC="CMA FreqAnalysis Coeff HL1 Setpoint",					PV_EGU="pu")
add_analog("CMA_FA_Coeff_HL2_S",		"REAL",		PV_DESC="CMA FreqAnalysis Coeff HL2 Setpoint",					PV_EGU="pu")
