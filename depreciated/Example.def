#Example
#2022-03-08
#
############################
#  DEFAULTS
############################ 
set_defaults(ARCHIVE=True)

############################
#  STATUS BLOCK
############################ 
define_status_block()

add_analog("Time_Stroke_Expected",    "REAL",    PV_DESC="Time stroke expected",              PV_EGU="s")
add_digital("MonitorSignal",                     PV_DESC="Monitor Signal - Processed",                       PV_ONAM="True", PV_ZNAM="False")
add_digital("ForceMode_Evan",                    PV_DESC="Force Mode Enabled",                               PV_ONAM="True", PV_ZNAM="False")
add_digital("ForceMode_Act",                     PV_DESC="Force Mode Act",                                   PV_ONAM="True", PV_ZNAM="False")
add_digital("Flt_NoFlowTimeOut_Lchd",            PV_DESC="Fault No-Flow TimeOut Latched",                    PV_ONAM="True", PV_ZNAM="False",                                           VALIDITY_PV="Fault_HWIO")
add_digital("Ctrl_DI_Flt_Lchd",                  PV_DESC="Control system DI fault latched",                  PV_ONAM="True", PV_ZNAM="False",             VALIDITY_CONDITION=False)
add_analog("Temperature_Meas",        "REAL",    PV_DESC="Temperature Measured",              PV_EGU="degC")
add_analog("Vol_Meas",                "REAL",    PV_DESC="Volume Measured",                   PV_EGU="cm3",                                                                             VALIDITY_PV="Fault_HWIO")
add_major_high_limit("Vol_H2_RB",     "REAL",    PV_DESC="Volume HL2",                        PV_EGU="cm3")
add_minor_high_limit("Vol_H1_RB",     "REAL",    PV_DESC="Volume HL1",                        PV_EGU="cm3")
add_minor_low_limit("Vol_L1_RB",      "REAL",    PV_DESC="Volume LL1",                        PV_EGU="cm3")
add_major_low_limit("Vol_L2_RB",      "REAL",    PV_DESC="Volume LL2",                        PV_EGU="cm3")
add_string("Vol_Meas", 39,                       PV_DESC="Volume Measured")
add_minor_alarm("Fault_HWIO",                    PV_DESC="Fault HWIO",                                       PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True)
add_major_alarm("Falult_PLC",                    PV_DESC="Falult_PLC",                                       PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True)
add_minor_alarm("Warning_LowGreaseLevel",        PV_DESC="Warning_LowGreaseLevel",                           PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="Fault_HWIO")
add_major_alarm("Falult_PLC2",                   PV_DESC="Falult_PLC2",                                      PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="Fault_HWIO")
add_analog("Vol_Calc",                "REAL",    PV_DESC="Volume Calculated",                 PV_EGU="cm3")






############################
#  COMMAND BLOCK
############################ 
define_command_block()

add_digital("Cmd_ForceVal1",                     PV_DESC="Command Force Value 1",                            PV_ONAM="True", PV_ZNAM="False")
add_digital("Cmd_ForceVal0",                     PV_DESC="Command Force Value 0",                            PV_ONAM="True", PV_ZNAM="False")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

add_digital("Force_Act",                         PV_DESC="Force Mode Active",                                PV_ONAM="True", PV_ZNAM="False")
add_analog("Vol_H2_SP",               "REAL",    PV_DESC="Volume HL2 Setpoint",               PV_EGU="cm3",                                                                             PV_PINI="YES", PV_VAL="[PLCF#Vol_H2_SP]")
