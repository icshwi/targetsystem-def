# Def file for "Proc-BA" device types of the Target System (accelerometers)
# Author:  Evan Foy 
# Date:    2021-09-30
# Version: v1.0		First version


############################
#  STATUS BLOCK
############################ 
define_status_block()
# HWIO Input signal
add_digital("HWIO_AI_Fault",				PV_DESC="HWIO Fault",          				PV_ONAM="True",				PV_ZNAM="False")
add_digital("Fault_HWIO_Lchd",				PV_DESC="Fault HWIO Latched",				PV_ONAM="True",				PV_ZNAM="False")

# Simulation mode
add_digital("SimMode_Act",						PV_DESC="Simulation Mode Active",			PV_ONAM="True",	PV_ZNAM="False")

# Validity signal
add_digital("Signal_Invalid",			PV_DESC="Signal Invalid",				PV_ONAM="NOK",			PV_ZNAM="OK",	VALIDITY_CONDITION=False)

# Vibration Assessment Results #
add_analog("Acc_RMS",				"REAL",		PV_DESC="Acceleration RMS",					PV_EGU="m/s2",		VALIDITY_PV="Signal_Invalid")
add_analog("Acc_RMS_Mean",			"REAL",		PV_DESC="Acceleration RMS Mean",			PV_EGU="m/s2",		VALIDITY_PV="Signal_Invalid")
add_analog("Acc_RMS_Std",			"REAL",		PV_DESC="Acceleration RMS Std",				PV_EGU="m/s2",		VALIDITY_PV="Signal_Invalid")
add_analog("Acc_RMS_MeanPlusStd",	"REAL",		PV_DESC="Acceleration RMS Mean Plus Std",	PV_EGU="m/s2",		VALIDITY_PV="Signal_Invalid")
add_analog("Acc_RMS_MeanSubStd",	"REAL",		PV_DESC="Acceleration RMS Mean Minus Std",	PV_EGU="m/s2",		VALIDITY_PV="Signal_Invalid")
add_analog("VibClass_VA_Acc",		"REAL",		PV_DESC="Vibr Class Vib Assessm Accel",		PV_EGU="Enum",		VALIDITY_PV="Signal_Invalid")

add_analog("Vel_RMS",				"REAL",		PV_DESC="Velocity RMS",						PV_EGU="m/s",		VALIDITY_PV="Signal_Invalid")
add_analog("Vel_RMS_Mean",			"REAL",		PV_DESC="Velocity RMS Mean",				PV_EGU="m/s",		VALIDITY_PV="Signal_Invalid")
add_analog("Vel_RMS_Std",			"REAL",		PV_DESC="Velocity RMS Std",					PV_EGU="m/s",		VALIDITY_PV="Signal_Invalid")
add_analog("Vel_RMS_MeanPlusStd",	"REAL",		PV_DESC="Velocity RMS Mean Plus Std",		PV_EGU="m/s",		VALIDITY_PV="Signal_Invalid")
add_analog("Vel_RMS_MeanSubStd",	"REAL",		PV_DESC="Velocity RMS Mean Minus Std",		PV_EGU="m/s",		VALIDITY_PV="Signal_Invalid")
add_analog("VibClass_VA_Vel",		"REAL",		PV_DESC="Vibr Class Vib Assessm Velocity",	PV_EGU="Enum",		VALIDITY_PV="Signal_Invalid")

add_analog("Disp_RMS",				"REAL",		PV_DESC="Deflection RMS",					PV_EGU="m",		    VALIDITY_PV="Signal_Invalid")
add_analog("Disp_RMS_Mean",			"REAL",		PV_DESC="Deflection RMS Mean",				PV_EGU="m",		    VALIDITY_PV="Signal_Invalid")
add_analog("Disp_RMS_Std",			"REAL",		PV_DESC="Deflection RMS Std",				PV_EGU="m",	    	VALIDITY_PV="Signal_Invalid")
add_analog("Disp_RMS_MeanPlusStd",	"REAL",		PV_DESC="Deflection RMS Mean Plus Std",		PV_EGU="m",	    	VALIDITY_PV="Signal_Invalid")
add_analog("Disp_RMS_MeanSubStd",	"REAL",		PV_DESC="Deflection RMS Mean Minus Std",	PV_EGU="m",		    VALIDITY_PV="Signal_Invalid")
add_analog("VibClass_VA_Disp",		"REAL",		PV_DESC="Vibr Class Vib Assessm Deflection",PV_EGU="Enum",		VALIDITY_PV="Signal_Invalid")

# Frequency Analysis Results #
add_analog("Power_MaxAmpl",			"REAL",		PV_DESC="Max Power Amplitude EnvSpectrum",	PV_EGU="m2/s4",		VALIDITY_PV="Signal_Invalid")
add_analog("Freq_AtMaxAmp",			"REAL",		PV_DESC="Frequency at Max Ampl EnvSpectrum",PV_EGU="Hz",		VALIDITY_PV="Signal_Invalid")
add_analog("VibClass_FA",			"REAL",		PV_DESC="Vibr Class Freq Analysis",			PV_EGU="Enum",		VALIDITY_PV="Signal_Invalid")
add_analog("Freq_AtMaxClass",		"REAL",		PV_DESC="Frequency at Max Vibr Class",		PV_EGU="Hz",		VALIDITY_PV="Signal_Invalid")

# Crest Factor Results #
add_analog("CrestFactor",			"REAL",		PV_DESC="Crest Factor",						PV_EGU="NA",		VALIDITY_PV="Signal_Invalid")



############################
#  COMMAND BLOCK
############################ 
define_command_block()
add_digital("Cmd_SimMode_On",			PV_DESC="Cmd Simulate On")
add_digital("Cmd_SimMode_Off",			PV_DESC="Cmd Simulate Off")

# Fault resets
add_digital("Cmd_Fault_RS",					PV_DESC="CMD: General Fault Reset")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

