# TMS_Proc_GU_MicroEpsILR
# 2022-11-11
#
############################
#  DEFAULTS
############################ 
#set_defaults(ARCHIVE=True)

############################
#  STATUS BLOCK
############################ 
define_status_block()

#   Device Raw Data
# -------------------------------------
add_minor_alarm("ProfibusErr_Act",     "NOK",    PV_DESC="Profibus Error Active",                                      PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True, ARCHIVE=True)
add_minor_alarm("Device_NOK",          "NOK",    PV_DESC="Device NOK",                                                 PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True, ARCHIVE=True)
add_digital("RawData_Valid",                     PV_DESC="RawData Valid",                                              PV_ONAM="OK",  PV_ZNAM="NOK",                VALIDITY_CONDITION=True, ARCHIVE=True)
add_analog("L_Raw_Profibus",           "REAL",   PV_DESC="Distance Raw Profibus",                       PV_EGU="mm",                                                                              VALIDITY_PV="RawData_Valid", ARCHIVE=True)
add_analog("T_Diode",                  "REAL",   PV_DESC="T Diode",                                     PV_EGU="degC",                                                                            VALIDITY_PV="RawData_Valid", ARCHIVE=True)
add_analog("Time_Operation",           "REAL",   PV_DESC="Time Operation",                              PV_EGU="Hr",                                                                              VALIDITY_PV="RawData_Valid", ARCHIVE=True)
add_digital("Device_On",                         PV_DESC="Device On",                                                  PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_minor_alarm("WarnBkgrndLight_Act", "NOK",    PV_DESC="Warning BkgrndLight Active",                                 PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="RawData_Valid", ARCHIVE=True)
add_minor_alarm("WarnDiodeTemp_Act",   "NOK",    PV_DESC="Warning DiodeTemperature Active",                            PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="RawData_Valid", ARCHIVE=True)
add_minor_alarm("WarnOther_Act",       "NOK",    PV_DESC="Warning Other Active",                                       PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="RawData_Valid", ARCHIVE=True)
add_minor_alarm("WarnStrongReflx_Act", "NOK",    PV_DESC="Warning StrongReflx Active",                                 PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="RawData_Valid", ARCHIVE=True)
add_minor_alarm("WarnWeakReflx_Act",   "NOK",    PV_DESC="Warning WeakReflx Active",                                   PV_ONAM="NOK", PV_ZNAM="OK",  ALARM_IF=True,                               VALIDITY_PV="RawData_Valid", ARCHIVE=True)

#   TMS Processed Data
# -------------------------------------
add_digital("MaskMode_Raw_Act",                  PV_DESC="MaskMode Raw Active",                                        PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("ProcData_Valid",                    PV_DESC="ProcData Valid",                                             PV_ONAM="OK",  PV_ZNAM="NOK",                VALIDITY_CONDITION=True, ARCHIVE=True)
add_analog("L_Raw_Used",               "REAL",   PV_DESC="Distance Raw Used",                           PV_EGU="mm",                                                                              VALIDITY_PV="ProcData_Valid", ARCHIVE=True)
add_string("Text_RawUsed_Source", 39,            PV_DESC="Text RawUsed Source", ARCHIVE=False)
add_digital("SampleHold_Act",                    PV_DESC="SampleHold Active",                                          PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("ManuOffset_Act",                    PV_DESC="ManuOffset Act",                                             PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_analog("L_OffsetManu_RB",          "REAL",   PV_DESC="Distance OffsetManu RB",                      PV_EGU="mm", ARCHIVE=True)
add_analog("L_Offset_RB",              "REAL",   PV_DESC="Distance Offset RB",                          PV_EGU="mm", ARCHIVE=True)
add_string("Text_Offset_Source", 39,             PV_DESC="Text Offset Source", ARCHIVE=False)
add_digital("MaskMode_Calc_Act",                 PV_DESC="MaskMode Calc Active",                                       PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("Fault",                             PV_DESC="Fault",                                                      PV_ONAM="True", PV_ZNAM="False",             VALIDITY_CONDITION=True, ARCHIVE=True)
add_analog("L_Calc_Used",              "REAL",   PV_DESC="Distance Calc Used",                          PV_EGU="mm",                                                                              VALIDITY_PV="Fault", ARCHIVE=True)
add_string("Text_CalcUsed_Source", 39,           PV_DESC="Text CalcUsed Source", ARCHIVE=False)
add_digital("Warning_Act",                       PV_DESC="Warning Summary",                                            PV_ONAM="True", PV_ZNAM="False",                                           VALIDITY_PV="Fault", ARCHIVE=True)
add_digital("Mask_Act",                          PV_DESC="Mask Summary",                                               PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)

############################
#  COMMAND BLOCK
############################ 
define_command_block()
add_digital("Cmd_PowerCycle",                    PV_DESC="Cmd PowerCycle",                                             PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)

#   TMS Data Processing Commands
# -------------------------------------
add_digital("Cmd_MaskRaw_Apply",                 PV_DESC="Cmd MaskRaw Apply",                                          PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("Cmd_SetNewOffset",                  PV_DESC="Cmd SetNewOffset",                                           PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("Cmd_ClearOffset",                   PV_DESC="Cmd ClearOffset",                                            PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("Cmd_MaskCalc_Apply",                PV_DESC="Cmd MaskCalc Apply",                                         PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)

add_digital("Cmd_SampleHold",                    PV_DESC="Cmd SampleHold",                                             PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("Cmd_SampleRelease",                 PV_DESC="Cmd SampleRelease",                                          PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("Cmd_SelectOffsetManu",              PV_DESC="Cmd SelectOffsetManu",                                       PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)
add_digital("Cmd_SelectOffsetSamp",              PV_DESC="Cmd SelectOffset Sampled",                                   PV_ONAM="True", PV_ZNAM="False", ARCHIVE=True)

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()
add_digital("MaskMode_Raw_S",                    PV_DESC="MaskMode Raw Param SetVal",                                  PV_ONAM="True", PV_ZNAM="False", ARCHIVE=False)
add_analog("L_Raw_Mask_S",             "REAL",   PV_DESC="Distance Raw Mask Param SetVal",              PV_EGU="mm", ARCHIVE=False)

add_digital("OffsetCtrl_Enbl_S",                 PV_DESC="OffsetCtrl Enbl Param SetVal",                               PV_ONAM="True", PV_ZNAM="False", ARCHIVE=False)
add_analog("L_RefVal_OffsCalc_S",      "REAL",   PV_DESC="L RefVal OffsCalc Param SetVal",              PV_EGU="mm", ARCHIVE=False)

add_digital("MaskMode_Calc_S",                   PV_DESC="MaskMode Calc Param SetVal",                                 PV_ONAM="True", PV_ZNAM="False", ARCHIVE=False)
add_analog("L_Calc_Mask_S",            "REAL",   PV_DESC="Distance Calc Mask Param SetVal",             PV_EGU="mm", ARCHIVE=False)

#   New
# -------------------------------------
add_analog("L_OffsetManu_S",           "REAL",   PV_DESC="Distance OffsetManu Param SetVal",            PV_EGU="mm", ARCHIVE=False)

